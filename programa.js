//L representa a la biblioteca leaflet
let miMapa = L.map("mapid");

//Determinar la vista inicial
miMapa.setView([4.800784, -74.345578], 17);
let miProveedor = L.tileLayer(
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
);


miProveedor.addTo(miMapa);

// marcador en el mapa

let miMarker=L.marker([4.800784, -74.345578]); 
        
        miMarker.addTo(miMapa);
       miMarker.bindPopup( "<b>Hola Mundo!</b><br />Agricultor:Wendy chamorro <br/>Planta:Arveja <br/> Fecha_plantacion: 2021/01/12"); 
        miMarker.openPopup();
        
  

let miGeoJSON= L.geoJSON(sitio);

miGeoJSON.addTo(miMapa);

let miTitulo=document.getElementById("titulo");
miTitulo.textContent=sitio.features[0].properties.popupContent;

let miDescripcion=document.getElementById("descripcion");
miDescripcion.textContent=sitio.features[0].properties.description;

